# frozen_string_literal: true

require 'spec_helper'
describe 'rails' do
  on_supported_os.each do |os, os_facts|
    let(:facts) do
      os_facts.merge(
        osfamily: 'Debian',
        operatingsystemrelease: '9.8',
      )
    end

    context "on #{os}" do
      context 'with default values for all parameters' do
        it { is_expected.to compile }
        it { is_expected.to contain_class('rails') }
        it { is_expected.to contain_class('ruby') }
        it { is_expected.to contain_class('ruby::dev') }
        it { is_expected.to contain_class('nodejs') }
        it { is_expected.to contain_class('yarn') }
        it { is_expected.to contain_class('capistrano') }
        it { is_expected.to contain_file('/var/www') }
      end

      context 'with user => deploy' do
        let(:params) do
          {
            user: 'deploy',
          }
        end

        it {
          is_expected.to contain_file('/var/www').with(
            owner: 'deploy',
          )
        }
      end

      context 'with deploy_root => /srv/apps' do
        let(:params) do
          {
            deploy_root: '/srv/apps',
          }
        end

        it { is_expected.to contain_file('/srv/apps') }
      end
    end
  end
end
