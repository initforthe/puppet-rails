# frozen_string_literal: true

require 'spec_helper'

describe 'rails::app' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:pre_condition) { 'include rails' }
      let(:title) { 'my_app' }
      let(:facts) { facts }

      context 'with default parameters' do
        it { is_expected.to contain_file('/var/www/my_app') }
        it { is_expected.to contain_file('/var/www/my_app/shared') }
        it { is_expected.to contain_file('/var/www/my_app/shared/config') }
        it { is_expected.to contain_file('/etc/systemd/system/my_app.service') }
        it { is_expected.to contain_file('/etc/systemd/system/my_app.socket') }
        it { is_expected.to contain_file('/etc/default/my_app') }

        it 'contains puma.rb' do
          is_expected.to contain_capistrano__shared_file('my_app-config/puma.rb').with(
            file_path: 'config/puma.rb',
            content: <<~EOF,
              workers 2
              threads = 5,5

              preload_app!

              pidfile '/var/www/my_app/shared/tmp/pids/puma.pid'
              state_path '/var/www/my_app/shared/tmp/pids/puma.state'
              bind 'unix:///var/www/my_app/shared/tmp/sockets/puma.socket'

              on_worker_boot do
                if defined?(ActiveRecord::Base)
                  ActiveSupport.on_load(:active_record) do
                    ActiveRecord::Base.establish_connection
                  end
                end
              end

              before_fork do
                if defined?(ActiveRecord::Base)
                  ActiveRecord::Base.connection_pool.disconnect!
                end
              end
            EOF
          )
        end
      end

      context "with :database_options => { 'adapter' => 'postgresql', 'database' => 'foo' }" do
        let(:params) do
          { database_options: {
            'adapter' => 'postgresql',
            'database' => 'foo',
          } }
        end

        it {
          is_expected.to create_capistrano__shared_file('my_app-config/database.yml').with(
            file_path: 'config/database.yml',
            content: %r{production:\n  adapter: postgresql\n  database: foo},
          )
        }
      end

      context 'with a mongdb database' do
        let(:params) do
          {
            'database_adapter' => 'mongodb',
            'database_options' => {
              'clients' => {
                'default' => {
                  'database' => 'my_app',
                  'hosts' => ['localhost:27017'],
                  'options' => {
                    'consistency' => '!ruby/sym strong',
                    'max_retries' => 30,
                    'retry_interval' => 1,
                    'timeout' => 30,
                  },
                },
              },
              'options' => {
                'raise_not_found_error' => false,
              },
            },
          }
        end

        it {
          is_expected.to create_capistrano__shared_file('my_app-config/mongoid.yml').with(
            content: <<~EOF,
              production:
                clients:
                  default:
                    database: my_app
                    hosts:
                    - localhost:27017
                    options:
                      consistency: :strong
                      max_retries: 30
                      retry_interval: 1
                      timeout: 30
                options:
                  raise_not_found_error: false

            EOF
          )
        }
        it { is_expected.not_to create_capistrano__shared_file('my_app-config/database.yml') }
      end
    end
  end
end
