# rails

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with rails](#setup)
    * [Beginning with rails](#beginning-with-rails)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module sets up a rails app ready for deployment via capistrano.

It assumes environment variables are loaded via
[dotenv-rails](https://github.com/bkeepers/dotenv) or similar.

## Setup

### Beginning with rails

The most simple usage is as follows:

```
include rails

rails::app { 'my_app': }
```

This will create a capistrano deployment in `/var/www/my_app` and put
environment variables in `/etc/defaults/my_app`.

## Usage

### Configure all rails deployments

```
class { 'rails':
  user        => 'deploy',
  deploy_root => '/srv/apps',
}
```

### Set up a rails app

```
rails::app { 'my_app':
  env => {
    'RAILS_ENV'       => 'staging',
    'SECRET_KEY_BASE' => '1234567890',
  }
}
```

## Reference


**Classes:**

* [rails](#rails)

**Defined Types:**

* [rails::app](#rails_app)

### Classes

#### rails

Sets up the root for all rails app deployments. Set the following parameters if
you want something other than the default settings:

##### `user`

Which user you want to own the deployments. The user must exist already.
Default: `root`

##### `deploy_root`

The root for your deployments. Default: `/var/www`

### Defined Types

#### rails::app

Sets up a deployment for a single app. Set the following parameters if you want
something other than the default settings:

##### `app_environment`

The environment the application should run under. Default: `production`

##### `db_opts`

A hash of options to be passed to the database.yml config file.

##### `env_vars`

A hash of environment variables to set for the app.
Default: `{}`

## Limitations

This module has only been tested on Debian-based OSes. YMMV with others.

## Development

Please follow the guidance for contributing on the [Puppet Forge](https://docs.puppet.com/forge/contributing.html)

### Getting things going

```
$ bundle install
$ gem install pdk
```

### Testing

```
$ pdk test unit
```

### Publishing **(from `master` only)**

Make sure everything is committed and pushed to master first. Direct changes to master should not be made.

For a minor version bump:
```
$ bundle exec rake module:release
```

For anything else:
```
$ bundle exec rake module:clean module:bump_commit:TYPE module:tag
$ git push && git push --tags
```

Details of type can be found by running `rake -T`
