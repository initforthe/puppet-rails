# See README.md for details
class rails (
  String $user                            = $rails::params::user,
  String $group                           = $rails::params::group,
  String $socket_user                     = $rails::params::socket_user,
  String $socket_group                    = $rails::params::socket_group,
  String $deploy_root                     = $rails::params::deploy_root,
  Boolean $system_dir_symlinks            = $rails::params::system_dir_symlinks,
  Optional[Array[String]] $extra_packages = $rails::params::extra_packages,
  Optional[Hash] $apps                    = undef,
) inherits rails::params {
  require ::ruby
  require ::ruby::dev
  require ::nodejs
  require ::yarn
  require ::logrotate
  require ::sudo

  ensure_packages($extra_packages, {'ensure' => 'present'})

  class { '::capistrano':
    user                => $user,
    group               => $group,
    deploy_root         => $deploy_root,
    system_dir_symlinks => $system_dir_symlinks,
  }

  if ! empty($apps) {
    create_resources('rails::app', $apps)
  }
}
