# Private params class
class rails::params {
  $user                = 'root'
  $group               = 'root'
  $socket_user         = 'root'
  $socket_group        = 'root'
  $deploy_root         = '/var/www'
  $system_dir_symlinks = false
  $extra_packages      = [
    'libxslt1-dev', 'libxml2-dev', 'zlib1g-dev', 'libffi-dev'
  ]
}
