# See README.md for details
define rails::app (
  Enum['present', 'absent'] $ensure         = present,
  String $app_environment                   = 'production',
  String $database_adapter                  = 'postgresql',
  Optional[Hash] $database_options          = undef,
  Optional[Hash] $env_vars                  = undef,
  Optional[Hash] $server_opts               = undef,
  Boolean $generate_puma_conf               = true,
  Boolean $use_socket                       = true,
  String $user                              = $rails::user,
  String $group                             = $rails::group,
  String $socket_user                       = $rails::socket_user,
  String $socket_group                      = $rails::socket_group,
  Optional[Array] $dependencies             = undef,
  Optional[Enum['sidekiq']] $worker_service = undef,
  Optional[String] $master_key              = lookup("rails::app::${title}::master_key", undef, undef, undef),
  Optional[Integer] $threads_min            = 5,
  Optional[Integer] $threads_max            = 5,
) {
  capistrano::deployment{ $title:
    ensure => $ensure,
  }

  if ($ensure == present) {
    if 'imagemagick' in $dependencies {
      if !defined(Package['imagemagick']) {
        ensure_packages(['imagemagick'])
      }
    }

    $database_ensure = $database_options ? {
      undef   => absent,
      default => present,
    }

    if $database_adapter in ['postgresql', 'mysql'] {
      if $database_adapter == 'postgresql' {
        include postgresql::lib::devel
      }

      $database_password = lookup("rails::apps::${title}::database_password", undef, undef, undef)

      capistrano::shared_file{ "${title}-config/database.yml":
        ensure    => $database_ensure,
        app       => $title,
        file_path => 'config/database.yml',
        content   => template('rails/database.yml.erb'),
        notify    => Service["${title}.service"],
      }
    }

    if $database_adapter == 'mongodb' {
      capistrano::shared_file{ "${title}-config/mongoid.yml":
        ensure    => $database_ensure,
        app       => $title,
        file_path => 'config/mongoid.yml',
        content   => template('rails/mongoid.yml.erb'),
        notify    => Service["${title}.service"],
      }
    }

    if $generate_puma_conf {
      capistrano::shared_file{ "${title}-config/puma.rb":
        ensure    => $ensure,
        app       => $title,
        file_path => 'config/puma.rb',
        content   => template('rails/config/puma.rb.erb'),
        notify    => Service["${title}.service"],
      }
    }

    if $master_key != undef {
      capistrano::shared_file{ "${title}-config/master.key":
        ensure    => $ensure,
        app       => $title,
        file_path => 'config/master.key',
        content   => $master_key,
        notify    => Service["${title}.service"],
      }
    }

    file { "/etc/default/${title}":
      ensure  => $ensure,
      content => template('rails/etc-default.erb'),
      notify  => Service["${title}.service"],
    }

    systemd::unit_file { "${title}.service":
      ensure  => $ensure,
      content => template('rails/app.service.erb'),
      enable  => true,
      active  => true,
    }

    if $use_socket {
      systemd::unit_file { "${title}.socket":
        ensure  => $ensure,
        content => template('rails/app.socket.erb'),
        enable  => true,
        active  => true,
      }
    }

    systemd::tmpfile { "${title}.conf":
      ensure  => $ensure,
      content => template('rails/app.tmpfile.erb'),
    }

    logrotate::rule { $title:
      path          => "${rails::deploy_root}/${title}/shared/log/*.log",
      rotate        => 7,
      rotate_every  => 'day',
      compress      => true,
      delaycompress => true,
      ifempty       => false,
      copytruncate  => true,
    }

    if defined('sudo::conf') and defined(Class['sudo']) {
      sudo::conf { $title:
        priority => 10,
        content  => "deploy ALL=(root) NOPASSWD:/usr/sbin/service ${title} restart",
      }
    }

    if $worker_service == 'sidekiq' {
      if !defined(Package['redis-server']) {
        ensure_packages(['redis-server'])
      }

      systemd::unit_file { "sidekiq_${title}.service":
        ensure  => $ensure,
        content => template('rails/sidekiq.service.erb'),
        enable  => true,
        active  => true,
      }

      if defined('sudo::conf') and defined(Class['sudo']) {
        sudo::conf { "sidekiq_${title}":
          priority => 10,
          content  => "deploy ALL=(root) NOPASSWD:/usr/sbin/service sidekiq_${title} restart",
        }
      }
    }
  }
}
